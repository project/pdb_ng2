# Decoupled Blocks: Angular 2

This is an [Angular 2](https://angular.io) implementation for the
[Decoupled Blocks](https://www.drupal.org/project/pdb) module. Blocks built
with Angular 2 can now encapsulate all that is needed for it and be added
to a site via a module or in a custom theme.

## Angular Version
This module only provides the Angular 2 version.

## Production Mode
Before turning on production mode in the module configuration form, make sure
to run `tsc` from the pdb_ng2 directory to compile your .ts files into .js.
